# W inclusive sample validation

### gridpack generation code

```sh
./gridpack_generation.sh wellnu01j_5f_LO_MLM cards/examples/wellnu01j_5f_LO_MLM
```

### validation code

```sh
# For W up to 1 jet
## For LO
./genval-run -g MGv2_wellnu01j_5f_LO_MLM_slc7_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz -f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max1j_LHE_pythia8_cff.py -n 1000 -j 1000 -d MGv2_wellnu01j_5f_LO_MLM_max1j -q condor -m madgraph -b 13600 -a W
## For NLO
./genval-run -g MGv2_wellnu01j_5f_NLO_FXFX_slc7_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz -f genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max1j_LHE_pythia8_cff.py -n 1000 -j 1000 -d MGv2_wellnu01j_5f_NLO_FXFX_max1j -q condor -m madgraph -b 13600 -a W

# For W up to 2 jet
## For LO generate 1M events
./genval-run -g MGv2_wellnu012j_5f_LO_MLM_slc7_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz -f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max2j_LHE_pythia8_cff.py -n 1000 -j 1000 -d MGv2_wellnu012j_5f_LO_MLM_max2j -q condor -m madgraph -b 13600 -a W

## For NLO generate 1M events [take MGv2 for example, MGv3 uses the same step and commands]
### for MGv* NLO gridpack generation, we removed the e in lepton definition and use loop_sm-no_b_mass model.
./genval-run -g MGv2_wellnu012j_5f_NLO_FXFX_slc7_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz -f genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_LHE_pythia8_cff.py -n 1000 -j 1000 -d MGv2_wellnu012j_5f_NLO_FXFX_max2j -q condor -m madgraph -b 13600 -a W

## after cmsenv
./genval-grid MGv2_wellnu012j_5f_NLO_FXFX_max2j

## calculate xsec
./genval-xsec -d MGv2_wellnu012j_5f_NLO_FXFX_max2j -j 5

cd MGv2_wellnu012j_5f_NLO_FXFX_max2j

## labelled with Jun12 means added MC_MET in W analysis
gunzip -c merged.yoda.gz > MGv2_W012j_NLO_Jun12.yoda
### in this yoda file, W related variables already have few non-zero values. 

## draw with rivet
export PYTHONPATH=/usr/local/lib/python3.10/site-packages:$PYTHONPATH
rivet-mkhtml --errs --no-weights MGv2_W012j_NLO_Jun12.yoda:Title="W+2j@NLO(v2918)" MGv3_W012j_NLO_Jun12.yoda:Title="W+2j@NLO(v352)" --remove-options -n 8 --output=output_W2Jet_NLO
### in .dat file created by rivet, W related variables also have few non-zero values. 
```


In process card, if we remove electron from lepton definition, we will get some weird W related plots.
This is likely due to rivet analysis code using electron to reconstruct W boson.
Therefore, in this case, we will get very few W boson events.

So this directory includes output with such weird results.
Just for the record.
